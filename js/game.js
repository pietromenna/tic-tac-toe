var Game = function(uiRefreshFunction, scoreBoard, canvasRenderer){
	var currentPlayer = "O";
	var board = [];
	var winnerPlayer = null;
	var currentValidMoves = 0;
	var scoreboard = scoreBoard;

	this.currentPlayer = function(){
		return currentPlayer;
	};

	this.play = function(move) {
		var maxValidMoves = 9; 
		if (play(move)){
			if (winnerPlayer != null){
				scoreboard.add(winnerPlayer);
				resetBoard();
			}
		}
		if (currentValidMoves >= maxValidMoves){
			resetBoard();
		}
		this.refreshUI(scoreboard, this);
		this.canvasRenderer.renderGame(this);
	};

	this.winnerPlayer = function() {
		return winnerPlayer;
	};

	this.getBoardValueAtPosition = function(position){
		if (position < 1 || position > 9 || 
			typeof board[(position - 1)] == "undefined" ) return null;
		return board[(position - 1)];
	};

	this.currentValidMoves = function(){
		return currentValidMoves;
	};

	this.refreshUI = uiRefreshFunction || function(){};
	this.canvasRenderer = canvasRenderer || {
		renderGame: function(game){},
		clear: function(){},
	};

	var play = function(move){
		var position = move - 1;
		if (!isValidMove(move)) return false;
		if (board[position] != null ) return false;
		board[position] = currentPlayer;
		if (isWinningMove()) {
			winnerPlayer = currentPlayer;
		}
		switchPlayer();
		currentValidMoves++;
		return true;
	};

	var switchPlayer = function() {
		if (currentPlayer == "O" ) {
			currentPlayer = "X";
		} else {
			currentPlayer = "O"
		}
	}

	var isValidMove = function(move){
		return !(move < 1 || move > 9);
	};

	var isWinningMove = function(){
		if (( board[0] == board[1] && board[2] == board[1] && board[0] == currentPlayer ) ||
		   ( board[3] == board[4] && board[4] == board[5] && board[3] == currentPlayer ) ||
		   ( board[6] == board[7] && board[7] == board[8] && board[6] == currentPlayer ) ||
		   ( board[0] == board[3] && board[3] == board[6] && board[0] == currentPlayer ) ||
		   ( board[1] == board[4] && board[4] == board[7] && board[1] == currentPlayer ) ||
		   ( board[2] == board[5] && board[5] == board[8] && board[2] == currentPlayer ) ||
		   ( board[0] == board[4] && board[4] == board[8] && board[0] == currentPlayer ) ||
		   ( board[6] == board[4] && board[4] == board[2] && board[6] == currentPlayer ))
		   return true;
		 return false;
	};

	var resetBoard = function(){
		for(var i=0;i<9;i++){
			board[i] = null;
		}
		currentValidMoves = 0;
		winnerPlayer =  null;
	};
};

var UpdateUI = function(scoreboard, game){
	var updateCurrentPlayer = function(game){
		var currentPlayer = document.getElementById("currentPlayer");
		currentPlayer.textContent = game.currentPlayer();
	};

	var updateScoreBoard = function(scoreboard){
		var score = document.getElementById("Player2Score");
		score.textContent = scoreboard.getScoreBoardFor("x");
		var score = document.getElementById("Player1Score");
		score.textContent = scoreboard.getScoreBoardFor("o");
	};

	var updateBoard = function(game){
		for (var i=1;i<10;i++) {
			var valueAtPos = game.getBoardValueAtPosition(i);
			var cell = document.getElementById(i);
			cell.textContent = valueAtPos;
		}
	};

	updateBoard(game);
	updateScoreBoard(scoreboard);
	updateCurrentPlayer(game);
};

var ScoreBoard = function(){
	var scores = {
		X: 0,
		O: 0,
	};

	this.getScoreBoardFor = function(player) {
		return scores[player.toUpperCase()];
	};

	this.add = function(player) {
		scores[player.toUpperCase()] = scores[player.toUpperCase()] + 1;
	};
};

var CanvasRenderer = function(){
	canvas = document.getElementById("canvas");
	context = canvas.getContext("2d");
	canvas_height = canvas.height;
	canvas_width = canvas.width;
};

CanvasRenderer.prototype = {
	clear: function() {
		context.clearRect(0, 0, canvas_width ,canvas_height);
	},

	drawLine: function(fromx, fromy, tox, toy){
		context.beginPath();
		context.moveTo(fromx,fromy);
		context.lineTo(tox, toy);
		context.stroke();
	},

	drawGrid: function(){
		context.lineWidth = 5;
		this.drawLine( 0,canvas_height / 3, canvas_width, canvas_height / 3 );
		this.drawLine( 0,2*canvas_height / 3, canvas_width, 2*canvas_height / 3 );
		this.drawLine( canvas_width / 3,0, canvas_width /3, canvas_height);
		this.drawLine( 2*canvas_width / 3,0, 2*canvas_width /3, canvas_height);
	},

	drawValueAtPos: function(value, position){
		var X = this.retrieveXof(position);
		var Y = this.retrieveYof(position);

		if (value === "O"){
			this.drawCircle(X, Y);
		} else if (value === "X"){
			this.drawCross(X, Y);
		} 
	},

	retrieveXof: function(position){
		if (position === 1 || position === 4 || position === 7 ) return 0;
		if (position === 2 || position === 5 || position === 8 ) return canvas_width/3;
		if (position === 3 || position === 6 || position === 9 ) return 2*canvas_width/3;
	},

	retrieveYof: function(position){
		if (position === 1 || position === 2 || position === 3 ) return 0;
		if (position === 4 || position === 5 || position === 6 ) return canvas_height/3;
		if (position === 7 || position === 8 || position === 9 ) return 2*canvas_height/3;
	},

	updateBoard: function(game) {
		for (var i=1;i<10;i++) {
			var valueAtPos = game.getBoardValueAtPosition(i);
			this.drawValueAtPos(valueAtPos, i);
		}
	},

	renderGame: function(game){
		this.clear();
		this.drawGrid();
		this.updateBoard(game);
	},

	drawCircle: function(x,y) {
		context.lineWidth = 5;
		context.beginPath();
		context.arc(x+canvas_width/6,y+canvas_height/6,canvas_height/8,0,2*Math.PI);
		context.stroke();
	},

	drawCross: function(x,y){
		context.lineWidth = 5;
		this.drawLine(x,y,x+canvas_width/3,y+canvas_height/3);
		this.drawLine(x,y+canvas_height/3,x+canvas_width/3,y);
	},
}

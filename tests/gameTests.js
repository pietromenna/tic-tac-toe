QUnit.test( "Game does not accept invalid moves", function( assert ) {
  game = new Game();
  var moves = game.currentValidMoves();
  game.play(0);
  assert.ok( moves == game.currentValidMoves(), "Invalid move!" );
});
QUnit.test( "On invalid move the player remains the same - First test", function( assert ) {
  game = new Game();
  var localCurrPlayer = game.currentPlayer();
  game.play(0);
  assert.ok( game.currentPlayer() == localCurrPlayer, "Invalid move!" );
});
QUnit.test( "On invalid move the player remains the same - Second test", function( assert ) {
  game = new Game();
  game.play(1);
  var localCurrPlayer = game.currentPlayer();
  game.play(1);
  assert.ok( game.currentPlayer() == localCurrPlayer, "Invalid move!" );
});
QUnit.test( "Game does not accept invalid moves - Test 2", function( assert ) {
  game = new Game();
  var moves = game.currentValidMoves();
  game.play(10);
  assert.ok( moves == game.currentValidMoves(), "Invalid move!" );
});
QUnit.test( "Game accepts valid move", function( assert ) {
  game = new Game();
  var moves = game.currentValidMoves();
  game.play(5);
  assert.ok( game.currentValidMoves() == (moves + 1), "This is a valid move!" );
});
QUnit.test( "Game does accepts valid move, but on used space", function( assert ) {
  game = new Game();
  game.play(5);
  var moves = game.currentValidMoves();
  game.play(5);
  assert.ok( game.currentValidMoves() == moves, "Should not accept used spaces as move!" );
});
QUnit.test( "Game switches to next player on success move", function( assert ) {
  game = new Game();
  var currentPlayer = game.currentPlayer();
  game.play(5);
  assert.ok( currentPlayer != game.currentPlayer(), "Player switch is not working!" );
});
QUnit.test( "Game switches to next player on success move - second case", function( assert ) {
  game = new Game();
  game.play(5);
  var currentPlayer = game.currentPlayer();
  game.play(6);
  assert.ok( currentPlayer != game.currentPlayer(), "Player switch is not working!" );
});
QUnit.test( "We can check for a winner - Just possible to call", function( assert ) {
  game = new Game();
  assert.ok( game.winnerPlayer() == null, "Problem while calling WinnerPlayer" );
});
QUnit.test( "We have a winner! - Mid Horizontal", function( assert ) {
  scoreboard = new ScoreBoard();
  game = new Game(null, scoreboard);
  var Oscore = scoreboard.getScoreBoardFor("O");
  game.play(5);
  game.play(7);
  game.play(6);
  game.play(8);
  game.play(4);
  assert.ok( scoreboard.getScoreBoardFor("O") == (Oscore + 1), "Problem to determine winnerPlayer" );
});
QUnit.test( "We have a winner! - Top Horizontal", function( assert ) {
  scoreboard = new ScoreBoard();
  game = new Game(null, scoreboard);
  var Oscore = scoreboard.getScoreBoardFor("O");
  game.play(1);
  game.play(4);
  game.play(2);
  game.play(5);
  game.play(3);
  assert.ok( scoreboard.getScoreBoardFor("O") == (Oscore + 1), "Problem to determine winnerPlayer" );
});
QUnit.test( "We have a winner! - Bottom Horizontal", function( assert ) {
  scoreboard = new ScoreBoard();
  game = new Game(null, scoreboard);
  var Oscore = scoreboard.getScoreBoardFor("O");
  game.play(7);
  game.play(4);
  game.play(8);
  game.play(5);
  game.play(9);
  assert.ok( scoreboard.getScoreBoardFor("O") == (Oscore + 1), "Problem to determine winnerPlayer" );
});
QUnit.test( "We have a winner! - Left Vertical", function( assert ) {
  scoreboard = new ScoreBoard();
  game = new Game(null, scoreboard);
  var Xscore = scoreboard.getScoreBoardFor("X");
  game.play(5);
  game.play(1);
  game.play(6);
  game.play(4);
  game.play(9);
  game.play(7);
  assert.ok( scoreboard.getScoreBoardFor("X") == (Xscore + 1), "Problem to determine winnerPlayer" );
});
QUnit.test( "We have a winner! - Mid Vertical", function( assert ) {
  scoreboard = new ScoreBoard();
  game = new Game(null, scoreboard);
  var Xscore = scoreboard.getScoreBoardFor("X");
  game.play(1);
  game.play(5);
  game.play(3);
  game.play(2);
  game.play(9);
  game.play(8);
  assert.ok( scoreboard.getScoreBoardFor("X") == (Xscore + 1), "Problem to determine winnerPlayer" );
});
QUnit.test( "We have a winner! - Right Vertical", function( assert ) {
  scoreboard = new ScoreBoard();
  game = new Game(null, scoreboard);
  var Oscore = scoreboard.getScoreBoardFor("O");
  game.play(3);
  game.play(1);
  game.play(6);
  game.play(2);
  game.play(9);
  assert.ok( scoreboard.getScoreBoardFor("O") == (Oscore + 1), "Problem to determine winnerPlayer" );
});
QUnit.test( "We have a winner! - Cross \\", function( assert ) {
  scoreboard = new ScoreBoard();
  game = new Game(null, scoreboard);
  var Oscore = scoreboard.getScoreBoardFor("O");
  game.play(5);
  game.play(2);
  game.play(1);
  game.play(4);
  game.play(9);
  assert.ok( scoreboard.getScoreBoardFor("O") == (Oscore + 1), "Problem to determine winnerPlayer" );
});
QUnit.test( "We have a winner! - Cross /", function( assert ) {
  scoreboard = new ScoreBoard();
  game = new Game(null, scoreboard);
  var Xscore = scoreboard.getScoreBoardFor("X");
  game.play(1);
  game.play(5);
  game.play(2);
  game.play(3);
  game.play(4);
  game.play(7);
  assert.ok( scoreboard.getScoreBoardFor("X") == (Xscore + 1), "Problem to determine winnerPlayer" );
});
QUnit.test( "Let`s have a draw?", function( assert ) {
  game = new Game();
  game.play(5);
  game.play(1);
  game.play(4);
  game.play(6);
  game.play(3);
  game.play(7);
  game.play(8);
  game.play(2);
  game.play(9);
  assert.ok( game.winnerPlayer() == null, "There is no winner in a draw" );
});
QUnit.test( "After a draw the next game starts with a different player", function( assert ) {
  game = new Game();
  var startingPlayer1 = game.currentPlayer();
  game.play(5);
  game.play(1);
  game.play(4);
  game.play(6);
  game.play(3);
  game.play(7);
  game.play(8);
  game.play(2);
  game.play(9);
  assert.ok( game.currentPlayer() != startingPlayer1, "Starting player did not change!" );
});
QUnit.test( "After a draw, all valid moves are valid again (board is cleared)", function( assert ) {
  game = new Game();
  var startingPlayer1 = game.currentPlayer();
  game.play(5);
  game.play(1);
  game.play(4);
  game.play(6);
  game.play(3);
  game.play(7);
  game.play(8);
  game.play(2);
  game.play(9); // Draw
  game.play(5);
  assert.ok( game.currentValidMoves() == 1, "Does not accept the move!" );
});
QUnit.test( "When there is a Win, it should update the scoreboard", function( assert ) {
  scoreboard = new ScoreBoardSpy();
  game = new Game(null, scoreboard);
  game.play(1);
  game.play(5);
  game.play(2);
  game.play(3);
  game.play(4);
  game.play(7);
  assert.ok( scoreboard.getScoreBoardFor("X") == 1, "Score did not change correctly!" );
});
QUnit.test( "During the game, after a win, there should not be winning player", function( assert ) {
  scoreboard = new DummyScoreBoard();
  game = new Game(null, scoreboard);
  game.play(1);
  game.play(5);
  game.play(2);
  game.play(3);
  game.play(4);
  game.play(7);
  game.play(1);
  assert.ok( game.winnerPlayer() == null, "Bug in which the winner player stays set!" );
});
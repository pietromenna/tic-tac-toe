QUnit.test( "New ScoreBoard begins with zero for both players", function( assert ) {
  scores = new ScoreBoard();
  assert.ok( scores.getScoreBoardFor("x") == 0, "Score do not start with 0" );
  assert.ok( scores.getScoreBoardFor("o") == 0, "Score do not start with 0" );
});
QUnit.test( "Scoreboard updates corretly when there is a winner", function( assert ) {
  scores = new ScoreBoard();
  var xScore = scores.getScoreBoardFor("x");
  var oScore = scores.getScoreBoardFor("o");
  scores.add("x");
  assert.ok( scores.getScoreBoardFor("x") == xScore + 1, "Score do not start with 0" );
  assert.ok( scores.getScoreBoardFor("o") == oScore, "Score do not start with 0" );
});

/*
Helper test Classes
*/
var DummyScoreBoard = function(){
	this.getScoreBoardFor = function(player) {
		return 0;
	};

	this.add = function(player) {
	};
};

var ScoreBoardSpy = function(){
	var score = 0;

	this.getScoreBoardFor = function() {
		return score;
	};

	this.add = function(player) {
		score = score + 1;
	};
};